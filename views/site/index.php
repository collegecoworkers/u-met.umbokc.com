<?php
$this->title = 'Туристические маршруты';
?>

<div id="map" class="map"></div>

<?= $this->render('../layouts/get-points', [
	'points' => $points
]); ?>
