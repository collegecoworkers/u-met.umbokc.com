<script>
	var placemarks = [
	<?php
	$r = "";
	foreach ($points as $item){
		$r .= "{latitude: {$item['latitude']}, longitude: {$item['longitude']},";
		$r .= "hintContent: '".$item['name']."', balloonContent: [\"";
		$r .= "<div style='overflow:auto'>";
		$r .= "<b>Название: </b>".$item['name']."<br>";
		$r .= "<b>Описание: </b>".trim($item['desc'])."<br>";
		$r .= "<img width=200 height=200 src='".$item['image']."'>";
		$r .= "</div>";
		$r .= "\"]},";
	}
	echo $r;
	?>
	];

</script>
