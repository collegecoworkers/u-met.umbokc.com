<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AdminAsset;

AdminAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">

	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

</head>
<body>
	<?php $this->beginBody() ?>

	<div class="wrapper-login">
		<?= $content ?>
	</div>

	<?= $this->render('footer.php', ['directoryAsset' => $directoryAsset]) ?>

</body>
</html>
<?php $this->endBody() ?>
<?php $this->endPage() ?>
