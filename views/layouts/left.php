	<div class="sidebar" data-background-color="white" data-active-color="danger">
		<div class="sidebar-wrapper">
			<div class="logo">
				<a href="/admin/" class="simple-text">
					Туристические маршруты
				</a>
			</div>

			<ul class="nav">
				<li class="active">
					<a href="/admin/">
						<i class="ti-panel"></i>
						<p>Админ панель</p>
					</a>
				</li>
				<li>
					<a href="/">
						<i class="ti-map"></i>
						<p>Перейти на сайт</p>
					</a>
				</li>
			</ul>
		</div>
	</div>
