<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
		<div class="page-inner login-page" >
			<div id="main-wrapper" class="container-fluid">
				<div class="row">
					<div class="col-md-6 col-sm-offset-3 login-box">
						<h4 class="login-title">Войти</h4>
						<?php $form = ActiveForm::begin(); ?>
							<div class="form-group">
								<?= $form->field($model, 'email')->textInput(['autofocus' => true, 'class' => 'form-control border-input']) ?>
							</div>
							<div class="form-group">
								<?= $form->field($model, 'password')->passwordInput(['class' => 'form-control border-input']) ?>
							</div>
							<button type="submit" class="btn btn-primary">Войти</button>
							<a href="/auth/signup" class="btn btn-default">Регистрация</a><br>
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
	</div>
</div>
