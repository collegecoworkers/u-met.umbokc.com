
<div class="page-inner">
	<div class="page-title">
		<a href="/admin/point" class="btn btn-primary">Добавить пункт</a>
		<h3 class="breadcrumb-header">Пункты</h3>
	</div>
	<div id="main-wrapper">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Данные</h4>
					</div>
					<div class="panel-body">
						<div class="content table-responsive table-full-width">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>#</th>
										<th>Название</th>
										<th>Широта</th>
										<th>Долгота</th>
										<th>Описание</th>
										<th>Действие</th>
									</tr>
								</thead>
								<tbody>
									<?php $i = 0; ?>
									<?php foreach ($points as $item): ?>
										<tr>
											<th scope="row"><?= $i ?></th>
											<td><?= $item['name'] ?></td>
											<td><?= $item['latitude'] ?></td>
											<td><?= $item['longitude'] ?></td>
											<td><?= $item['desc'] ?></td>
											<td>
												<a href="/admin/point-edit/?id=<?= $item['id'] ?>">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="/admin/point-delete/?id=<?= $item['id'] ?>" onclick="return confirm('Вы уверены?')">
													<i class="fa fa-trash"></i>
												</a>
											</td>
										</tr>
										<?php ++$i; ?>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- Row -->
	</div><!-- Main Wrapper -->
</div><!-- /Page Inner -->
