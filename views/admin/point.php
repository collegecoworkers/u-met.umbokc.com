<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\Point;

$this->title = $model->isNewRecord ? 'Новый пункт' : 'Изменить пункт';
?>

<!-- Page Inner -->
<div class="page-inner">
	<div class="page-title">
		<h3 class="breadcrumb-header"><?= $this->title ?></h3>
	</div>
	<div id="main-wrapper">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<form id="w0" enctype="multipart/form-data" action="<?= $model->isNewRecord ? '/admin/point' : '/admin/point-edit/?id='.$model->id ?>" method="post">
							<div class="form-group">
								<input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->csrfToken?>"/>
								<input type="text" class="form-control border-input" name="Point[name]" placeholder="Название"
								value="<?= $model->isNewRecord ? '' : $model->name ?>">
							</div>
							<div class="form-group">
								<textarea class="form-control border-input" name="Point[desc]" placeholder="Название"><?= $model->isNewRecord ? '' : $model->desc ?></textarea>
							</div>
							<div class="form-group field-fileupload-file required has-error">
								<label class="control-label" for="fileupload-file">Изображение</label>
								<input type="hidden" name="FileUpload[image]" value="">
								<input type="file" id="fileupload-file" name="FileUpload[image]" aria-required="true" aria-invalid="true">
							</div>
							<div class="row">
								<div class="form-group col-md-6">
									<input type="text" class="form-control border-input" name="Point[latitude]" placeholder="Широта"
									value="<?= $model->isNewRecord ? '' : $model->latitude ?>">
								</div>
								<div class="form-group col-md-6">
									<input type="text" class="form-control border-input" name="Point[longitude]" placeholder="Долгота"
									value="<?= $model->isNewRecord ? '' : $model->longitude ?>">
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Сохранить</button>
							</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->render('../layouts/get-points', [
					'points' => $points
					]); ?>
				<div id="map"></div>
			</div>
		</div>
	</div>
</div>
