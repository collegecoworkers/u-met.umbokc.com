<?php
use yii\db\Migration;

class m180202_074132_point extends Migration
{

	public function up()
	{
		$this->createTable('point', [
			'id' => $this->primaryKey(),
			'name'=>$this->string(),
			'desc'=>$this->text(),
			'image'=>$this->string(),
			'latitude'=>$this->string(),
			'longitude'=>$this->string(),
		]);
	}

	public function down()
	{
		$this->dropTable('point');
	}
}
