<?php
use yii\db\Migration;

class m180202_074132_user extends Migration
{

	public function up()
	{
		$this->createTable('user', [
			'id' => $this->primaryKey(),
			'name'=>$this->string(),
			'email'=>$this->string()->defaultValue(null),
			'password'=>$this->string(),
			'isAdmin'=>$this->integer()->defaultValue(0),
		]);
	}

	public function down()
	{
		$this->dropTable('user');
	}
}
