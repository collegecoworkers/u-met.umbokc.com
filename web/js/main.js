ymaps.ready(init);

function dbg(mes){
	console.log(mes);
	window.a = mes;
}

var geoObjects= [];

function init() {
	var myMap = new ymaps.Map('map', {
		center: [55.730, 37.9974],
		zoom: 13,
		controls: ['zoomControl', 'routePanelControl'],
		// behaviors: ['drag']
	}, {
		balloonMaxWidth: 200,
		searchControlProvider: 'yandex#search'
	});

	for (var i = 0; i < placemarks.length; i++) {
		var placemark = new ymaps.Placemark([placemarks[i].latitude, placemarks[i].longitude], {
			hintContent: placemarks[i].hintContent,
			balloonContent: placemarks[i].balloonContent.join(''),
			// iconContent: "Азербайджан"
		}, {
			preset: "twirl#yellowStretchyIcon",
			iconLayout: 'default#image',
			iconImageHref: '/img/entity.png',
			iconImageSize: [48, 48],
			iconImageOffset: [-24, -24],
		});

		myMap.geoObjects.add(placemark);
	}

	U.control = myMap.controls.get('routePanelControl');

	var control_routePanel_is_visible = !true;

	U.control.routePanel.state.set({
		type: 'masstransit',
		fromEnabled: true,
		toEnabled: true
	});

	// Зададим опции панели для построения машрутов.
	U.control.routePanel.options.set({
		// Запрещаем показ кнопки, позволяющей менять местами начальную и конечную точки маршрута.
		allowSwitch: !false,
		// Включим определение адреса по координатам клика.
		// Адрес будет автоматически подставляться в поле ввода на панели, а также в подпись метки маршрута.
		reverseGeocoding: true,
		// Зададим виды маршрутизации, которые будут доступны пользователям для выбора.
		types: { masstransit: true, pedestrian: true }
	});

	var switchPointsButton = new ymaps.control.Button({
		data: {content: "Постоить маршрут", title: "Постоить маршрут"},
		options: {selectOnClick: false, maxWidth: 160}
	});

	switchPointsButton.events.add('click', function () {
		control_routePanel_is_visible = !control_routePanel_is_visible;

		if (control_routePanel_is_visible) {
			switchPointsButton.data.set('content','Скрыть');
			switchPointsButton.data.set('title','Скрыть');
		} else {
			switchPointsButton.data.set('content','Постоить маршрут');
			switchPointsButton.data.set('title','Постоить маршрут');
		}

		U.control._setVisibleState(control_routePanel_is_visible);
		// control.routePanel.switchPoints();
	});

	myMap.controls.add(switchPointsButton);
	// U.timer("U.toggleClass(U.control._layout.getParentElement(), 'ymaps-2-1-64-controls__control_visibility_hidden');", 1000);
	U.timer('U.control._setVisibleState(false);', 1000);
}
