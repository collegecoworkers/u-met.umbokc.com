<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

use app\models\Point;
use app\models\FileUpload;

class AdminController extends Controller
{

	public function behaviors()
	{
		return [
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function beforeAction($action)
	{
		if (Yii::$app->user->isGuest) {
			return $this->redirect('/auth/login');
		}
		return parent::beforeAction($action);
	}

	public function actionIndex()
	{
		return $this->redirect('/admin/points');
	}

	public function actionPoints()
	{
		$points = Point::getAll();
		return $this->render('index', [
			'points' => $points,
		]);
	}

	public function actionPoint()
	{

		$model = new Point();
		$image = new FileUpload;

		if(Yii::$app->request->post()){
			$data = Yii::$app->request->post()['Point'];
			$model->name = $data['name'];
			$model->desc = $data['desc'];
			$model->name = $data['name'];
			$model->latitude = $data['latitude'];
			$model->longitude = $data['longitude'];

			if(UploadedFile::getInstance($image, 'image'))
				$model->image = $image->uploadFile(UploadedFile::getInstance($image, 'image'));

			$model->save();
			return $this->redirect('/admin/points');
		}

		return $this->render('point', [
			'model' => $model,
			'points' => Point::getAll(),
		]);
	}

	public function actionPointEdit($id)
	{

		$model = Point::findOne($id);
		$image = new FileUpload;

		if(Yii::$app->request->post()){
			$data = Yii::$app->request->post()['Point'];
			$model->name = $data['name'];
			$model->desc = $data['desc'];
			$model->latitude = $data['latitude'];
			$model->longitude = $data['longitude'];

			if(UploadedFile::getInstance($image, 'image'))
				$model->image = $image->uploadFile(UploadedFile::getInstance($image, 'image'));

			$model->save();
			return $this->redirect('/admin/points');
		}

		return $this->render('point', [
			'model' => $model,
			'points' => Point::getAll(),
		]);
	}

	public function actionPointDelete($id)
	{
		Point::findOne($id)->delete();
		return $this->redirect(['/admin/index']);
	}
}
