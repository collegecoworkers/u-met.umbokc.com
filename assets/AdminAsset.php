<?php

namespace app\assets;

use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        '/public/assets/css/bootstrap.min.css',
        '/public/assets/css/animate.min.css',
        '/public/assets/css/paper-dashboard.css',
        '/public/assets/css/demo.css',
        'http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css',
        'https://fonts.googleapis.com/css?family=Muli:400,300',
        '/public/assets/css/themify-icons.css',
    ];
    public $js = [
        '/public/assets/js/jquery-1.10.2.js',
        '/public/assets/js/bootstrap.min.js',
        '/public/assets/js/paper-dashboard.js',
        '/public/assets/js/demo.js',
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
        '/js/admin.js',
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
