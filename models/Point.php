<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class Point extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'point';
	}

	public function rules()
	{
		return [
			[['image', 'desc', 'latitude', 'longitude', 'name'], 'string'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название',
			'desc' => 'Описание',
			'image' => 'Изображение',
			'latitude' => 'Широта',
			'longitude' => 'Долгота',
		];
	}

	public static function findIdentity($id)
	{
		return self::findOne($id);
	}

	public static function getAll()
	{
		$points = [];

		$all = self::find()->all();

		foreach ($all as $item) {
			$points[] = [
				'id' => $item->id,
				'name' => $item->name,
				'desc' => $item->desc,
				'image' => $item->image != '' ? '/uploads/' . $item->image : '',
				'latitude' => $item->latitude,
				'longitude' => $item->longitude,
			];
		}
		return $points;
	}

}
